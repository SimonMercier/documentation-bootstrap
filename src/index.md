---
home: true
heroImage: /welcome-image.png
tagline: Welcome
actionText: Get started →
actionLink: /menu1/
features:
- title: Feature 1 Title
  details: Feature 1 Description
- title: Feature 2 Title
  details: Feature 2 Description
- title: Feature 3 Title
  details: Feature 3 Description
footer: Made by ... with ❤️
---
