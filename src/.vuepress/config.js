const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Awesome doc',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#0C9D46' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    logo: '/welcome-image.png',
    docsDir: 'src',
    docsBranch: 'master',
    editLinks: true,
    editLinkText: 'Help us improve this page!',
    lastUpdated: true,
    search: false,
    sidebarDepth: 0,
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Menu 1', link: '/menu1/' },
      { text: 'Menu 2', link: '/menu2/' }
    ],
    sidebar: {
      '/menu1/': [    
        ['', 'Menu 1 Home'],
        ['sub', 'Menu 1 sub'], 
        {
          title: 'Menu 1 sub collapsable',
          collapsable: true,
          children: [
            '/menu1/collapsable/collapsable-sub-1',
            '/menu1/collapsable/collapsable-sub-2',
          ]
        }
      ],

      '/menu2/': [],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ],

  /**
   * Deployment，ref：https://v1.vuepress.vuejs.org/guide/deploy.html#gitlab-pages-and-gitlab-ci
   */
  base: '/documentation-bootstrap/',
  dest: 'public'
}
